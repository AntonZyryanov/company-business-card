import Foundation
import SwiftUI


class OnlineShop {
    
    @State static  var shared: OnlineShop = OnlineShop()
    
    
    
   // @EnvironmentObject var userSettings: OnlineShopUserSettings
    
    var themeMainColor: Color = .red
    static var themeSecondaryColor: Color = .white
    static var themeThirdColor: Color = .orange
    static var productsBackgroundColor: Color = .white
    static var headersFont: Font = .custom("Avenir-Book", size: 40)
    static var miniHeadersFont: Font = .custom("Avenir-Book", size: 24)
    static var textFont: Font = .custom("Avenir-Book", size: 18)
    
    @State var currentUser : MUser = MUser(username: "_", email: "_", avatarStringURL: "_", description: "_", sex: "_", id: "_")
    @State var chatWithConsultant : MChat = MChat(friendUsername: "_", friendAvatarStringURL: "_", friendId: "Consultant1", lastMessageContent: "")
    
    @State var consultant : MUser = MUser(username: "_", email: "consultant1@cbc.com", avatarStringURL: "_", description: "_", sex: "_", id: "consultant1@cbc.com")
    
    @Published var chosenProducts: [ProductsFeedItem] = []
    
    @Published var photoArchive: [Int:[Photo]] = [:]
    
    @Published var numberOfChosenProducts: Int = 0
    
    @Published var totalPrice: Double = 0
    
    @State var addToCartDelegate: addToCartCellDelegate?
    
    @EnvironmentObject var onlineShopUserSettings: OnlineShopUserSettings
    
    var view: some View {
        TabView {
           Products()
             .tabItem {
                 Image(systemName: "bag")
                 Text("Products")
             }
           Cart()
             .tabItem {
                Image(systemName: "cart")
                Text("Cart")
           }
           Chat()
              .tabItem {
                 Image(systemName: "bubble.left")
                 Text("Service support")
           }
            Settings()
                 .tabItem {
                    Image(systemName: "gear")
                    Text("Settings")
              }
        }.accentColor(themeMainColor)
            
    }
}
