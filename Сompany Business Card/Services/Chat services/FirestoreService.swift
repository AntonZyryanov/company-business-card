import Firebase
import FirebaseFirestore
import UIKit

class FirestoreService {
    
    static let shared = FirestoreService()
    
    let db = Firestore.firestore()
    
    private var usersRef: CollectionReference {
        return db.collection("users")
    }
    
    private var productsRef: CollectionReference {
        return db.collection("products")
    }
    
    private var waitingChatsRef: CollectionReference {
        return db.collection(["users", currentUser.email, "waitingChats"].joined(separator: "/"))
    }
    
    private var activeChatsRef: CollectionReference {
        return db.collection(["users", currentUser.email, "activeChats"].joined(separator: "/"))
    }
    
    var currentUser: MUser!
    
    func getUserData(user: User, completion: @escaping (Result<MUser, Error>) -> Void) {
        guard let email = user.email else {return}
        let docRef = usersRef.document(email)
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                guard let muser = MUser(document: document) else {
                    completion(.failure(UserError.cannotUnwrapToMUser))
                    return 
                }
                self.currentUser = muser
                completion(.success(muser))
            } else {
                completion(.failure(UserError.cannotGetUserInfo))
            }
        }
    }
    
    func setCurrentUser (currentUser: MUser) {
        self.currentUser = currentUser
    }
    
    func saveProfileWith(email: String, completion: @escaping (Result<MUser, Error>) -> Void) {
        
        var muser = MUser(username: "_",
                          email: email,
                          avatarStringURL: "not exist",
                          description: "_",
                          sex: "_",
                          id: email)
        muser.avatarStringURL = "_"
        self.usersRef.document(muser.id).setData(muser.representation) { (error) in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(muser))
            }
        }
    } // saveProfileWith
    
    func createWaitingChat(message: String, receiver: MUser, completion: @escaping (Result<Void, Error>) -> Void) {
        let reference = db.collection(["users", receiver.id, "waitingChats"].joined(separator: "/"))
        let messageRef = reference.document(self.currentUser.id).collection("messages")
        
        let message = MMessage(user: currentUser, content: message)
        let chat = MChat(friendUsername: currentUser.username,
                         friendAvatarStringURL: currentUser.avatarStringURL,
                         friendId: currentUser.id, lastMessageContent: message.content)
        
        reference.document(currentUser.id).setData(chat.representation) { (error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            messageRef.addDocument(data: message.representation) { (error) in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                completion(.success(Void()))
            }
        }
    }
    
    func deleteWaitingChat(chat: MChat, completion: @escaping (Result<Void, Error>) -> Void) {
        waitingChatsRef.document(chat.friendId).delete { (error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            self.deleteMessages(chat: chat, completion: completion)
        }
    }
    
    func deleteMessages(chat: MChat, completion: @escaping (Result<Void, Error>) -> Void) {
        let reference = waitingChatsRef.document(chat.friendId).collection("messages")
        
        getWaitingChatMessages(chat: chat) { (result) in
            switch result {
                
            case .success(let messages):
                for message in messages {
                    guard let documentId = message.id else { return }
                    let messageRef = reference.document(documentId)
                    messageRef.delete { (error) in
                        if let error = error {
                            completion(.failure(error))
                            return
                        }
                        completion(.success(Void()))
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getWaitingChatMessages(chat: MChat, completion: @escaping (Result<[MMessage], Error>) -> Void) {
        let reference = waitingChatsRef.document(chat.friendId).collection("messages")
        var messages = [MMessage]()
        reference.getDocuments { (querySnapshot, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            for document in querySnapshot!.documents {
                guard let message = MMessage(document: document) else { return }
                messages.append(message)
            }
            completion(.success(messages))
        }
    }
    
    func changeToActive(chat: MChat, completion: @escaping (Result<Void, Error>) -> Void) {
        getWaitingChatMessages(chat: chat) { (result) in
            switch result {
            case .success(let messages):
                self.deleteWaitingChat(chat: chat) { (result) in
                    switch result {
                    case .success:
                        self.createActiveChat(chat: chat, messages: messages) { (result) in
                            switch result {
                            case .success:
                                completion(.success(Void()))
                            case .failure(let error):
                                completion(.failure(error))
                            }
                        }
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func createActiveChat(chat: MChat, messages: [MMessage], completion: @escaping (Result<Void, Error>) -> Void) {
        let messageRef = activeChatsRef.document(chat.friendId).collection("messages")
        activeChatsRef.document(chat.friendId).setData(chat.representation) { (error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            for message in messages {
                messageRef.addDocument(data: message.representation) { (error) in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                    completion(.success(Void()))
                }
            }
        }
    }
    
    func sendMessage(chat: MChat, message: MMessage, completion: @escaping (Result<Void, Error>) -> Void) {
        let friendRef = usersRef.document(chat.friendId).collection("activeChats").document(currentUser.id)
        let friendMessageRef = friendRef.collection("messages")
        let myMessageRef = usersRef.document(currentUser.id).collection("activeChats").document(chat.friendId).collection("messages")
        
        let chatForFriend = MChat(friendUsername: currentUser.username,
                                  friendAvatarStringURL: currentUser.avatarStringURL,
                                  friendId: currentUser.id,
                                  lastMessageContent: message.content)
        friendRef.setData(chatForFriend.representation) { (error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            friendMessageRef.addDocument(data: message.representation) { (error) in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                myMessageRef.addDocument(data: message.representation) { (error) in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                    completion(.success(Void()))
                }
            }
        }
    }
    
    
    // MARK: -/-
    
    func getProducts(completion: @escaping (Result<[ProductsFeedItem], Error>) -> Void) {
        let concurrentQueue = DispatchQueue(label: "com.gcd.dispatchBarrier", attributes: .concurrent)
        let reference = productsRef
        var response: [ProductsFeedItem]  = []
        concurrentQueue.sync(flags: .barrier) {
            reference.getDocuments { (querySnapshot, error) in
                
                
                
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        guard let id = data["id"] as? Int else { return  }
                        guard let name = data["name"] as? String else { return  }
                        print("OBI-WAN : \(name)")
                        self.getPhotosForProduct(withId: id) { result in
                            switch result {
                            case .success(let photos):
                                print("Photos count: \(photos.count)")
                                guard let responseFromFB = ProductsFeedItem(document: document, photos: photos) else { return }
                                
                                response.append(responseFromFB)
                                print("JAWA")
                                print("Productz count: \(response.count)")
                                
                                print("Products count: \(response.count)")
                                completion(.success(response))
                            case .failure(_):
                                print("Failed loading photos")
                            }
                        }
                        
                        
                    }
                    
                    
                
                
            }
        }
        concurrentQueue.sync(flags: .barrier) {
            
        }
        
        
    }
    
    func getPhotosForProduct(withId id: Int, completion: @escaping (Result<[Photo], Error>) -> Void) {
        
        let photosRef = db.collection(["products", String(id), "photos"].joined(separator: "/"))
        var response: [Photo]  = []
        
        photosRef.getDocuments { (querySnapshot, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            for document in querySnapshot!.documents {
                print("C3PO")
                let data = document.data()
                guard let width = data["width"] as? Int else {
                    print("Vader")
                    return  }
                print("PLO-KOON : \(width)")
                guard let responseFromFB = Photo(document: document) else { return }
                
                
                response.append(responseFromFB)
            }
            completion(.success(response))
        }
    }
}
