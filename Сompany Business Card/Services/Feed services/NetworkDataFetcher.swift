//
//  NetworkDataFetcher.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 09/03/2019.
//  Copyright © 2019 Алексей Пархоменко. All rights reserved.
//

import Foundation

protocol DataFetcher {
    func getFeed(response: @escaping (ProductsFeedResponse?) -> Void)
    func getUser(response: @escaping (UserResponse?) -> Void)
}

struct NetworkDataFetcher: DataFetcher {
    
    static var loadedProducts: [ProductsFeedItem] = []
    
    let networking: Networking
    
    init(networking: Networking) {
        self.networking = networking
    }
    
    func getUser(response: @escaping (UserResponse?) -> Void) {
        
    }
    
    func getFeed(response: @escaping (ProductsFeedResponse?) -> Void) {
        DispatchQueue.global(qos: .userInteractive).sync {
            FirestoreService.shared.getProducts { result in
                switch result {
                case .success(let products):
                    print("YANIX count \(products.count)")
                    NetworkDataFetcher.loadedProducts = products
                    let productsFeedResponse: ProductsFeedResponse = ProductsFeedResponse(items: NetworkDataFetcher.loadedProducts)
                    response(productsFeedResponse)
                case .failure(_):
                    print("getFeed Worker failed")
                }
            }
            
        }
        
        
    }
    
    private func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let data = from, let response = try? decoder.decode(type.self, from: data) else { return nil }
        return response
    }
}
