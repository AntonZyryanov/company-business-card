//
//  FeedResponse.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 08/03/2019.
//  Copyright © 2019 Алексей Пархоменко. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct ProductsFeedResponse: Decodable {
    var items: [ProductsFeedItem]
}

struct ProductsFeedItem: Decodable {
    let id: Int
    let name: String
    let text: String?
    let price: Double
    let photos: [Photo]?
    
    init?(document: QueryDocumentSnapshot,photos: [Photo]) {
        let data = document.data()
        guard let id = data["id"] as? Int else { return nil }
        print("BUBA 1")
        guard let name = data["name"] as? String else {return nil}
        print("BUBA 2")
        guard let text = data["text"] as? String else {return nil}
        print("BUBA 3")
        guard let price = data["price"] as? Double else { return nil }
        print("BUBA 4")
        
        self.id = id
        self.name = name
        self.text = text
        self.price = price
        self.photos = photos
    }
    
    init?(id: Int,name: String,text: String?,price: Double,photos: [Photo]?) {
        self.id = id
        self.name = name
        self.text = text
        self.price = price
        self.photos = photos
    }
}

struct Photo: Decodable {
    let url: String
    let width: Int
    let height: Int
    
    init?(document: QueryDocumentSnapshot) {
        let data = document.data()
        guard let url = data["url"] as? String else { return nil }
        guard let width = data["width"] as? Int else {return nil}
        guard let height = data["height"] as? Int else {return nil}
        
        self.url = url
        self.width = width
        self.height = height
    }
    
    init?(url: String, width: Int, height: Int) {
        self.url = url
        self.width = width
        self.height = height
    }
}
