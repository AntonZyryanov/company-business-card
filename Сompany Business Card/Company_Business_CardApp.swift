//
//  _ompany_Business_CardApp.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 17.01.2022.
//

import SwiftUI

@main
struct Company_Business_CardApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
