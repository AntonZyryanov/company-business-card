//
//  ContentView.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 24.01.2022.
//

import SwiftUI
import FirebaseAuth

struct ContentView: View {
    
    @State  var isAuthenticationNeeded = true
    
    @StateObject var onlineShopUserSettings = OnlineShopUserSettings()
    
    var body: some View {
        OnlineShop.shared.view
            .fullScreenCover(isPresented: $onlineShopUserSettings.isAuthenticationNeeded, content: Authentication.init)
            .environmentObject(onlineShopUserSettings)
            .onAppear {
                
                if let user = Auth.auth().currentUser {
                    FirestoreService.shared.getUserData(user: user) { (result) in
                        switch result {
                        case .success(let muser):
                            print("Biggie : \(muser)")
                            self.onlineShopUserSettings.currentUser = muser
                            print("Biggie2 : \(self.onlineShopUserSettings.currentUser )")
                            self.onlineShopUserSettings.isAuthenticationNeeded = false
                            self.onlineShopUserSettings.loggedIn = true
                            self.onlineShopUserSettings.addToCartDelegate = CartFeedViewController.shared
                        case .failure(_):
                            self.onlineShopUserSettings.isAuthenticationNeeded = true
                        }
                    }
                } else {
                    self.onlineShopUserSettings.isAuthenticationNeeded = true
                    print("Failed to auth")
                }
                }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
