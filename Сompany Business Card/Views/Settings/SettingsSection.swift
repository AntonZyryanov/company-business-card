//
//  SettingsSection.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 24.01.2022.
//

import SwiftUI

struct SettingsSection: View {
    
    var color: Color = .white
    
    var miniHeadersFont = SettingsMediator.miniHeadersFont
    var textFont = SettingsMediator.textFont
    
    init (text:String,subText: String,color: Color,miniHeadersFont: Font,textFont: Font){
        self.text = text
        self.subText = subText
        self.color = color
        self.miniHeadersFont = miniHeadersFont
        self.textFont = textFont
    }
    
    var text: String  = ""
    
    var subText: String = ""
    
    var body: some View {
        VStack{
            HStack{
                Spacer().frame(width:20)
                Text(text).font(miniHeadersFont).foregroundColor(color)
                Spacer()
            }
            
            Spacer().frame(height: 20)
            HStack{
                Spacer().frame(width: 20)
                
                if text == "LEGAL"{
                    VStack{
                        HStack{
                            Spacer().frame(width:20)
                            Text("PRIVACY POLICY").font(textFont).foregroundColor(color)
                            Spacer()
                        }
                        HStack{
                            Spacer().frame(width:20)
                            Text("TERMS AND CONDITIONS").font(textFont).foregroundColor(color)
                            Spacer()
                        }   
                    }
                }else{
                    HStack{
                        Spacer().frame(width:20)
                        Text(subText).font(textFont).foregroundColor(color)
                        Spacer()
                    }
                    
                }
            }
        }
        
    }
}

struct SettingsSection_Previews: PreviewProvider {
    static var previews: some View {
      //  SettingsSection()
        Text("")
    }
}
