import SwiftUI
import FirebaseAuth
import FirebaseFirestore

struct Settings: View {
    
    var themeMainColor: Color = SettingsMediator.themeMainColor
    var themeSecondaryColor: Color = SettingsMediator.themeSecondaryColor
    
    var headersFont = SettingsMediator.headersFont
    var miniHeadersFont = SettingsMediator.miniHeadersFont
    var textFont = SettingsMediator.textFont
    
    @State var signOut: Bool = false
    
    @EnvironmentObject var userSettings: OnlineShopUserSettings
    
    
    var settingsSections: [(String,String)]  = [("FEEDBACK","FEEDBACKTEMPLATE@GMAIL.COM"), ("APPSTORE","GO TO APPSTORE"),("RATE","RATE THE APP"),("LEGAL","")]
    
    var body: some View {
        ZStack{
            themeMainColor.ignoresSafeArea()
            VStack{
                
                ScrollView{
                    HStack{
                        Spacer().frame(width: 20)
                        SettingsHeader(color: themeSecondaryColor)
                        Spacer()
                        Button(action: {
                                signOut = true
                            }) {
                                Image(systemName: "person.crop.circle.badge.xmark").frame(width: 40, height: 40).foregroundColor(themeSecondaryColor)
                            }
                        Spacer().frame(width: 20)
                    }
                    
                    Spacer().frame(height:40)
                    ForEach(0..<settingsSections.count){index in
                        SettingsSection(text: settingsSections[index].0, subText: settingsSections[index].1,color: themeSecondaryColor,miniHeadersFont: miniHeadersFont,textFont: textFont)
                    
                    }
                    
                    
                }.alert(isPresented: $signOut) { () -> Alert in
                    Alert(title: Text("Signing out"), message: Text("Are you sure?"), primaryButton: .default(Text("Yes"), action: {
                        do {
                            try Auth.auth().signOut()
                            userSettings.loggedIn = false
                            userSettings.isAuthenticationNeeded = true
                        } catch {
                            print("Error signing out: \(error.localizedDescription)")
                        }
                    }), secondaryButton: .default(Text("Cancel")))
            }
                Spacer()
            }
        }
    }
}

struct Settings_Previews: PreviewProvider {
    static var previews: some View {
        Settings()
    }
}
