//
//  MessageView.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 24.01.2022.
//

import SwiftUI

struct MessageView: View {
    
    init (message: Message) {
        self.message = message
    }
    
    var message: Message = Message(sender: .user, text: "", isWithImage: false, imageName: nil)
    
    var body: some View {
        if message.sender == .company {
            HStack{
                ZStack{
                    Color.white.ignoresSafeArea()
                    VStack{
                        Text(message.text)
                    }
                }.frame(width: CGFloat(message.text.count*10), height: 50)
                Spacer()
            }
        }else{
            HStack{
                Spacer()
                ZStack{
                    Color.white.ignoresSafeArea()
                    VStack{
                        Text(message.text)
                    }
                }.frame(width: CGFloat(message.text.count*10), height: 50)
            }
        }
        
    }
}

struct MessageView_Previews: PreviewProvider {
    static var previews: some View {
       // MessageView()
        Text("")
    }
}
