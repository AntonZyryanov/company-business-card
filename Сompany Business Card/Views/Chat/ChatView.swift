//
//  ChatView.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 03.02.2022.
//

import SwiftUI

struct ChatView: View {
    
    
    
    init (chat: MChat){
        self.chat = chat
    }
    
    var chat: MChat = MChat(friendUsername: "", friendAvatarStringURL: "", friendId: "", lastMessageContent: "")
    
    var themeMainColor: Color = ChatMediator.themeMainColor
    var themeSecondaryColor: Color = ChatMediator.themeSecondaryColor
    
    var miniHeadersFont = ChatMediator.miniHeadersFont
    var textFont = ChatMediator.textFont
    
    var body: some View {
        HStack{
            Spacer().frame(width:20)
            Image(systemName: "person.crop.circle").foregroundColor(themeSecondaryColor).scaleEffect(3)
            Spacer().frame(width:20)
            VStack{
                Text(chat.friendId).foregroundColor(themeSecondaryColor).font(miniHeadersFont)
               // Text(chat.lastMessageContent).foregroundColor(themeSecondaryColor).font(textFont)
            }
            Spacer()
        }.frame(height:90)
    }
}

struct ChatView_Previews: PreviewProvider {
    static var previews: some View {
       // ChatView()
        Text("")
    }
}
