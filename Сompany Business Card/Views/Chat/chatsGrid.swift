//
//  chatsGrid.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 03.02.2022.
//

import SwiftUI
import Firebase
import FirebaseAuth
import FirebaseFirestore

struct chatsGrid: View {
    
    @State var showCurrentChat: Bool = false
    
    @EnvironmentObject var onlineShopUserSettings: OnlineShopUserSettings
    
    @State var currentChat: MChat = MChat(friendUsername: "_", friendAvatarStringURL: "_", friendId: "", lastMessageContent: "")
    
    @State var activeChats = [MChat]()
    
    @State private var activeChatsListener: ListenerRegistration?
    
    init () {
        
    }
    
    
    var themeMainColor: Color = ChatMediator.themeMainColor
    var themeSecondaryColor: Color = ChatMediator.themeSecondaryColor
    
    var data = (1...100).map { "Item \($0)" }

        let columns = [
            GridItem(.flexible())
        ]

        var body: some View {
            ZStack{
                themeMainColor.ignoresSafeArea()
                ScrollView {
                    LazyVGrid(columns: columns, spacing: 20) {
                        ForEach(activeChats, id: \.self) { item in
                            Button(action: {
                                    onlineShopUserSettings.currentChat = item
                                    showCurrentChat = true
                                }) {
                                    ChatView(chat: item)
                                }
                        }
                    }
                    .padding(.horizontal)
                }
            }
            .fullScreenCover(isPresented: $showCurrentChat, onDismiss: nil) {
                Messenger(showCurrentChat: $showCurrentChat)
            }
            .onAppear {
                ListenerService.shared.setCurrentUserId(id: onlineShopUserSettings.currentUser.id)
                activeChatsListener = ListenerService.shared.activeChatsObserve(chats: activeChats, completion: { (result) in
                    switch result {
                    case .success(let chats):
                        self.activeChats = chats
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                })
            }
            .onDisappear {
                print("Kizaru")
                activeChatsListener?.remove()
                ListenerService.shared.setCurrentUserId(id: "")
            }
        }
}

struct chatsGrid_Previews: PreviewProvider {
    static var previews: some View {
        chatsGrid()
    }
}
