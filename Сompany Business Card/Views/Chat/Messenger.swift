import UIKit
import SwiftUI
import MessageKit

// 1.
struct Messenger: UIViewControllerRepresentable {
    
    @EnvironmentObject var userSettings: OnlineShopUserSettings
    
    @Binding var showCurrentChat : Bool
    
    @State var currentChat = MChat(friendUsername: "", friendAvatarStringURL: "", friendId: "", lastMessageContent: "")

    // 2.
    func makeUIViewController(context: Context) -> UINavigationController {
     /*   print(OnlineShop.shared.currentUser)
        print(OnlineShop.shared.chatWithConsultant)
        return ChatsViewController(user: OnlineShop.shared.currentUser, chat: OnlineShop.shared.chatWithConsultant) */
        let chatsVC = ChatsViewController(user: userSettings.currentUser, chat: userSettings.currentChat)
        chatsVC.userSettings = userSettings
        let navigationVC = UINavigationController(rootViewController: chatsVC)
        return navigationVC
    }
    
    // 3.
    func updateUIViewController(_ uiViewController: UINavigationController, context: Context) {
        print("")
    }
}
