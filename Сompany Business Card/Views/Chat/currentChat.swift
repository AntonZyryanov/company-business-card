//
//  currentChat.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 03.02.2022.
//

import SwiftUI

struct currentChat: View {
    
    @Binding var showCurrentChat : Bool
    
    var body: some View {
        NavigationView {
                    ScrollView {
                        Messenger(showCurrentChat: $showCurrentChat)
                    }
                    .navigationBarTitle("Try it!", displayMode: .inline)
                    .background(NavigationConfigurator { nc in
                        nc.navigationBar.barTintColor = .blue
                        nc.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white]
                    })
                }
            .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct currentChat_Previews: PreviewProvider {
    static var previews: some View {
      //  currentChat()
        Text("")
    }
}



struct NavigationConfigurator: UIViewControllerRepresentable {
    var configure: (UINavigationController) -> Void = { _ in }

    func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
        UIViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
        if let nc = uiViewController.navigationController {
            self.configure(nc)
        }
    }

}
