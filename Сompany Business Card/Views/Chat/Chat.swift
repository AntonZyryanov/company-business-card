//
//  Chat.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 19.01.2022.
//

import SwiftUI

struct Chat: View {
    
    var themeMainColor: Color = ChatMediator.themeMainColor
    var themeSecondaryColor: Color = ChatMediator.themeSecondaryColor
    
    var miniHeadersFont = ChatMediator.miniHeadersFont
    var textFont = ChatMediator.textFont
    
    @State var userLogedIn : Bool = false
    
    @EnvironmentObject var userSettings: OnlineShopUserSettings
    
    var body: some View {
        ZStack{
            themeMainColor.ignoresSafeArea()
            VStack{
                ChatHeader(color: themeSecondaryColor)
                if userSettings.loggedIn {
                    chatsGrid()
                }else{
                    Text("Chats loading...")
                        .foregroundColor(themeSecondaryColor)
                        .font(miniHeadersFont)
                }
            }
            
        }
    }
}

struct Chat_Previews: PreviewProvider {
    static var previews: some View {
        Chat()
    }
}
