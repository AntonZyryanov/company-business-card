//
//  ChatHeader.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 24.01.2022.
//

import SwiftUI

struct ChatHeader: View {
    init (color: Color) {
        self.textColor = color
    }
    
    var textColor: Color = .white
    
    var body: some View {
        HStack{
            Spacer().frame(width: 20)
            Text("Service support").foregroundColor(textColor).font(.custom("Avenir-Book", size: 40))
            Spacer()
        }
    }
}

struct ChatHeader_Previews: PreviewProvider {
    static var previews: some View {
        //ChatHeader()
        Text("")
    }
}
