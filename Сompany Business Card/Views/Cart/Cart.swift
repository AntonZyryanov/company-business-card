import SwiftUI

struct Cart: View {
    
    var themeMainColor: Color = ChatMediator.themeMainColor
    var themeSecondaryColor: Color = CartMediator.themeSecondaryColor
    var themeThirdColor: Color = CartMediator.themeThirdColor
    var productsBackgroundColor: Color = CartMediator.productsBackgroundColor
    
    @EnvironmentObject var userSettings: OnlineShopUserSettings
    
    var body: some View {
        ZStack{
            themeThirdColor.ignoresSafeArea()
            VStack{
                ZStack{
                    themeMainColor.ignoresSafeArea()
                    CartHeader(textColor: themeSecondaryColor, themeColor: themeMainColor, chosenProductsAmount: 0)
                    
                }.frame(height: 90)
                ZStack{
                    themeThirdColor.ignoresSafeArea()
                    VStack{
                        CartTotal().frame(height: 100)
                        CartTableView()
                    }
                }.padding(0)
                
                    
            }
        }
        
    }
}

struct Cart_Previews: PreviewProvider {
    static var previews: some View {
        Cart()
    }
}
