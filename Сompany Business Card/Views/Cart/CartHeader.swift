//
//  CartHeader.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 24.01.2022.
//

import SwiftUI

struct CartHeader: View {
    init (textColor: Color,themeColor: Color,chosenProductsAmount:Int) {
        self.textColor = textColor
        self.chosenProductsAmount = chosenProductsAmount
        self.themeColor = themeColor
    }
    
    var chosenProductsAmount: Int = 0
    
    var textColor: Color = .white
    
    var themeColor: Color = .red
    
    var body: some View {
        ZStack{
            themeColor.ignoresSafeArea()
            VStack{
                HStack{
                    Spacer().frame(width:20)
                    Text("Cart").foregroundColor(textColor).font(.custom("Avenir-Book", size: 40))
                    Spacer()
                }
                
                Spacer().frame(height:40)
           /*     HStack{
                    Text("You chosen \(chosenProductsAmount) items").foregroundColor(textColor).font(.custom("Avenir-Book", size: 18)).padding(.leading, 20)
                    Spacer()
                }   */
                
            }
        }
    }
}

struct CartHeader_Previews: PreviewProvider {
    static var previews: some View {
       // CartHeader()
       Text("")
    }
}
