//
//  CartTotal.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 01.03.2022.
//

import SwiftUI

struct CartTotal: View {
    
    @State var numberOfItems = OnlineShop.shared.numberOfChosenProducts
    @State var totalPrice = OnlineShop.shared.totalPrice
    
    var body: some View {
        ZStack{
            CartMediator.themeThirdColor.ignoresSafeArea()
            VStack{
                Text("You chosen \(numberOfItems) items").foregroundColor(.white).font(Font.system(size:30, design: .default))
                Text("Total price: \(totalPrice, specifier: "%.2f") $").foregroundColor(.white).font(Font.system(size:20, design: .default))
                    ZStack{
                        CartMediator.themeMainColor.ignoresSafeArea()
                        HStack{
                            Text("Purchase").foregroundColor(.white).font(Font.system(size:20, design: .default))
                        }
                    }.frame(width: 150, height: 40,alignment: .center).cornerRadius(10)
                Spacer().frame(height: 20)
            }
            
        }.onAppear {
            numberOfItems = OnlineShop.shared.numberOfChosenProducts
            totalPrice = OnlineShop.shared.totalPrice
        }
    }
}

struct CartTotal_Previews: PreviewProvider {
    static var previews: some View {
        CartTotal()
    }
}
