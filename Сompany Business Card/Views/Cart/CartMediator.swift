import Foundation
import SwiftUI

class CartMediator {
    
    static var themeMainColor = OnlineShop.shared.themeMainColor
    static var themeSecondaryColor = OnlineShop.themeSecondaryColor
    static var themeThirdColor = OnlineShop.themeThirdColor
    static var productsBackgroundColor = OnlineShop.productsBackgroundColor
    
    static var headersFont = OnlineShop.headersFont
    static var miniHeadersFont = OnlineShop.miniHeadersFont
    static var textFont = OnlineShop.textFont

}

