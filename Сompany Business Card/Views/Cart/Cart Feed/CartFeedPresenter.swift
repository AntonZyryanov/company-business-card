//
//  NewsfeedPresenter.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 15/03/2019.
//  Copyright (c) 2019 Алексей Пархоменко. All rights reserved.
//

import UIKit

protocol CartFeedPresentationLogic {
  func presentData(response: CartFeed.Model.Response.ResponseType)
}

class CartFeedPresenter: CartFeedPresentationLogic {
    
    weak var viewController: CartFeedDisplayLogic?
    var cellLayoutCalculator: CartFeedCellLayoutCalculatorProtocol = CartFeedCellLayoutCalculator()
    
    let dateFormatter: DateFormatter = {
       let dt = DateFormatter()
        dt.locale = Locale(identifier: "ru_RU")
        dt.dateFormat = "d MMM 'в' HH:mm"
        return dt
    }()
  
  func presentData(response: CartFeed.Model.Response.ResponseType) {
  
    switch response {
    case .presentProducts(let feed, let revealdedPostIds):
        print("JANGO FET \(feed.items.count)")
        let cells = feed.items.map { (feedItem) in
            cellViewModel(from: feedItem, revealdedPostIds: revealdedPostIds)
        }
        
        let footerTitle = ""
        let feedViewModel = CartFeedViewModel.init(cells: cells, footerTitle: footerTitle)
        
        print("BOBBA FET \(feedViewModel.cells.count)")
        viewController?.displayData(viewModel: CartFeed.Model.ViewModel.ViewModelData.displayNewsfeed(feedViewModel: feedViewModel))
    case .presentFooterLoader:
        viewController?.displayData(viewModel: CartFeed.Model.ViewModel.ViewModelData.displayFooterLoader)
    }
  }
    
    private func cellViewModel(from feedItem: ProductsFeedItem,revealdedPostIds: [Int]) -> CartFeedViewModel.CartCell {
        
        let photoAttachments = self.photoAttachments(feedItem: feedItem)
        
        let isFullSized = revealdedPostIds.contains { (id) -> Bool in
            return id == feedItem.id
        }
        
        let sizes = cellLayoutCalculator.sizes(postText: feedItem.text, photoAttachments: photoAttachments, isFullSizedPost: isFullSized)
        
        let postText = feedItem.text?.replacingOccurrences(of: "<br>", with: "\n")
        
        return CartFeedViewModel.CartCell.init(id: feedItem.id, name: feedItem.name, text: postText, price: feedItem.price, photoAttachements: photoAttachments, sizes: sizes)
    }
    
    private func formattedCounter(_ counter: Int?) -> String? {
        guard let counter = counter, counter > 0 else { return nil }
        var counterString = String(counter)
        if 4...6 ~= counterString.count {
            counterString = String(counterString.dropLast(3)) + "K"
        } else if counterString.count > 6 {
            counterString = String(counterString.dropLast(6)) + "M"
        }
        return counterString
    }
        
    private func photoAttachments(feedItem: ProductsFeedItem) -> [CartFeedViewModel.CartFeedCellPhotoAttachment] {
        guard let attachments = feedItem.photos else { return [] }
        
        return attachments.compactMap({ (attachment) -> CartFeedViewModel.CartFeedCellPhotoAttachment? in
            let photo = attachment
            return CartFeedViewModel.CartFeedCellPhotoAttachment.init(photoUrlString: photo.url,
                                                              width: photo.width,
                                                              height: photo.height)
        })
    }
}
