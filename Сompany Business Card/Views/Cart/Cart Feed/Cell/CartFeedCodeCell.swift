//
//  NewsfeedCodeCell.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 31/03/2019.
//  Copyright © 2019 Алексей Пархоменко. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI
import SnapKit

protocol CartFeedCodeCellDelegate: class {
    func revealPost(for cell: CartFeedCodeCell)
}

final class CartFeedCodeCell: UITableViewCell {
    
    static let reuseId = "NewsfeedCodeCell"
    
    weak var delegate: CartFeedCodeCellDelegate?
    
    // Первый слой
    
    let cardView: UIView = {
       let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // Второй слой
    
    let topView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let postlabel: UITextView = {
       let textView = UITextView()
        textView.font = CartFeedConstants.postLabelFont
        textView.isScrollEnabled = false
        textView.isSelectable = true
        textView.isUserInteractionEnabled = true
        textView.isEditable = false
        
        let padding = textView.textContainer.lineFragmentPadding
        textView.textContainerInset = UIEdgeInsets.init(top: 0, left: -padding, bottom: 0, right: -padding)
        
        textView.dataDetectorTypes = UIDataDetectorTypes.all
        return textView
    }()
    
    lazy var moreTextButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        button.setTitleColor(UIColor(ProductsMediator.themeMainColor), for: .normal)
        button.contentHorizontalAlignment = .left
        button.contentVerticalAlignment = .center
        button.setTitle("show in full...", for: .normal)
        
        return button
    }()
    
    let galleryCollectionView = CartGalleryCollectionView()
    
    let postImageView: WebImageView = {
        let imageView = WebImageView()
        imageView.backgroundColor = #colorLiteral(red: 0.8234507442, green: 0.3115251064, blue: 0.3296223879, alpha: 1)
        return imageView
    }()
    
    let bottomView: UIView = {
       let view = UIView()
        return view
    }()
    
    // Третий слой на topView
    
    
    let nameLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 0.227329582, green: 0.2323184013, blue: 0.2370472848, alpha: 1)
        return label
    }()
    
    // Третий слой на bottomView
    
    let priceLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.numberOfLines = 1
        label.backgroundColor = .orange
        label.textColor = UIColor(ProductsMediator.themeSecondaryColor)
        label.textAlignment = .center
        return label
    }()
    
    let addToCardButton: UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        button.titleLabel?.numberOfLines = 1
        button.backgroundColor = UIColor(ProductsMediator.themeMainColor)
        button.titleLabel?.textColor = UIColor(ProductsMediator.themeSecondaryColor)
        button.setTitle("Remove from cart", for: .normal)
        return button
    }()
    
    let cartImageView: UIImageView = {
       let cartImageView = UIImageView()
        cartImageView.image = UIImage(systemName: "cart.fill")
        cartImageView.tintColor = UIColor(ProductsMediator.themeSecondaryColor)
        return cartImageView
    }()
    
    
    override func prepareForReuse() {
        postImageView.set(imageURL: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        backgroundColor = .clear
        selectionStyle = .none
        
        cardView.layer.cornerRadius = 10
        cardView.clipsToBounds = true
        overlayFirstLayer() // первый слой
        overlaySecondLayer() // второй слой
        overlayThirdLayerOnTopView() // третий слой на topView
        overlayThirdLayerOnBottomView() // третий слой на bottomView
        overlayFourthLayerOnBottomViewViews() // четвертый слой на bottomViewViews
        moreTextButton.addTarget(self, action: #selector(moreTextButtonTouch), for: .touchUpInside)
        self.isUserInteractionEnabled = true
        self.cardView.isUserInteractionEnabled = true
        self.moreTextButton.isUserInteractionEnabled = true
        self.moreTextButton.isEnabled = true
        
        
        self.isUserInteractionEnabled = true
        self.cardView.isUserInteractionEnabled = true
    }
    
    @objc func moreTextButtonTouch() {
        print("Nemtsov")
        delegate?.revealPost(for: self)
    }
    
    @objc func buttonTouch() {
        print("Nemtsov")
        delegate?.revealPost(for: self)
    }
    
    func set(viewModel: CartFeedCellViewModel) {
        
        nameLabel.text = viewModel.name
        postlabel.text = viewModel.text
        
        postlabel.frame = viewModel.sizes.postLabelFrame
        
        bottomView.frame = viewModel.sizes.bottomViewFrame
        moreTextButton.frame = viewModel.sizes.moreTextButtonFrame
        
        priceLabel.text = String(viewModel.price) + " $"
        
        if let photoAttachment = viewModel.photoAttachements.first, viewModel.photoAttachements.count == 1 {
            postImageView.isHidden = false
            galleryCollectionView.isHidden = true
            postImageView.set(imageURL: photoAttachment.photoUrlString)
            postImageView.frame = viewModel.sizes.attachmentFrame
        } else if viewModel.photoAttachements.count > 1 {
            postImageView.isHidden = true
            galleryCollectionView.isHidden = false
            galleryCollectionView.frame = viewModel.sizes.attachmentFrame
            galleryCollectionView.set(photos: viewModel.photoAttachements)
        }
        else {
            postImageView.isHidden = true
            galleryCollectionView.isHidden = true
        }
    }
    
    private func overlayFourthLayerOnBottomViewViews() {
        addToCardButton.addSubview(cartImageView)
        cartImageView.snp.makeConstraints { make in
            make.height.equalTo(18)
            make.width.equalTo(18)
            make.trailing.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
        }
        
        addToCardButton.titleLabel?.snp.makeConstraints({ make in
            make.leading.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
        })
    }
    

    
    private func overlayThirdLayerOnBottomView() {
        
    }
    
    private func overlayThirdLayerOnTopView() {
        topView.addSubview(nameLabel)
        
        // nameLabel constraints
        nameLabel.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 0).isActive = true
        nameLabel.topAnchor.constraint(equalTo: topView.topAnchor, constant: 12).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: CartFeedConstants.topViewHeight / 2 - 2).isActive = true
        bottomView.addSubview(priceLabel)
        bottomView.addSubview(addToCardButton)
        
        priceLabel.layer.cornerRadius = 10
        priceLabel.clipsToBounds = true
        
        priceLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
            make.width.equalTo(120)
            make.height.equalTo(40)
        }
        
        addToCardButton.layer.cornerRadius = 10
        addToCardButton.clipsToBounds = true

        addToCardButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
            make.width.equalTo(200)
            make.height.equalTo(40)
        }
    }
    
    
    
    private func overlaySecondLayer() {
        cardView.addSubview(topView)
        cardView.addSubview(postlabel)
        cardView.addSubview(moreTextButton)
        cardView.addSubview(postImageView)
        cardView.addSubview(galleryCollectionView)
        cardView.addSubview(bottomView)
        
        
        // topView constraints
        topView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        topView.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        topView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 8).isActive = true
        topView.heightAnchor.constraint(equalToConstant: CartFeedConstants.topViewHeight).isActive = true

        
        // postlabel constraints
        // не нужно, так как размеры задаются динамически
        
        // moreTextButton constraints
        // не нужно, так как размеры задаются динамически
        
        // postImageView constraints
        // не нужно, так как размеры задаются динамически
        
        // bottomView constraints
        // не нужно, так как размеры задаются динамически
    }
    
     private func overlayFirstLayer() {
       // addSubview(cardView)
         contentView.addSubview(cardView)
        
        // cardView constraints
        cardView.fillSuperview(padding: CartFeedConstants.cardInsets)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
