//
//  NewsfeedCellLayoutCalculator.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 27/03/2019.
//  Copyright © 2019 Алексей Пархоменко. All rights reserved.
//

import Foundation
import UIKit

struct CartSizes: CartFeedCellSizes {
    var postLabelFrame: CGRect
    var moreTextButtonFrame: CGRect
    var attachmentFrame: CGRect
    var bottomViewFrame: CGRect
    var totalHeight: CGFloat
}

protocol CartFeedCellLayoutCalculatorProtocol {
    func sizes(postText: String?, photoAttachments: [CartFeedCellPhotoAttachementViewModel], isFullSizedPost: Bool) -> CartFeedCellSizes
}

final class CartFeedCellLayoutCalculator: CartFeedCellLayoutCalculatorProtocol {
    
    private let screenWidth: CGFloat
    
    init(screenWidth: CGFloat = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)) {
        self.screenWidth = screenWidth
    }

    func sizes(postText: String?, photoAttachments: [CartFeedCellPhotoAttachementViewModel], isFullSizedPost: Bool) -> CartFeedCellSizes {
        
        var showMoreTextButton = false
        
        let cardViewWidth = screenWidth - CartFeedConstants.cardInsets.left - CartFeedConstants.cardInsets.right
        
        // MARK: Работа с postLabelFrame
        
        var postLabelFrame = CGRect(origin: CGPoint(x: CartFeedConstants.postLabelInsets.left, y: CartFeedConstants.postLabelInsets.top),
                                    size: CGSize.zero)
        
        if let text = postText, !text.isEmpty {
            let width = cardViewWidth - CartFeedConstants.postLabelInsets.left - CartFeedConstants.postLabelInsets.right
            var height = text.height(width: width, font: CartFeedConstants.postLabelFont)
            
            let limitHeight = CartFeedConstants.postLabelFont.lineHeight * CartFeedConstants.minifiedPostLimitLines
            
            if !isFullSizedPost && height > limitHeight {
                height = CartFeedConstants.postLabelFont.lineHeight * CartFeedConstants.minifiedPostLines
                showMoreTextButton = true
            }
        
            postLabelFrame.size = CGSize(width: width, height: height)
        }
        
        // MARK: Работа с moreTextButtonFrame
        
        var moreTextButtonSize = CGSize.zero
        
        if showMoreTextButton {
            moreTextButtonSize = CartFeedConstants.moreTextButtonSize
        }
        
        let moreTextButtonOrigin = CGPoint(x: CartFeedConstants.moreTextButtonInsets.left, y: postLabelFrame.maxY)
        
        let moreTextButtonFrame = CGRect(origin: moreTextButtonOrigin, size: moreTextButtonSize)
        
        
        // MARK: Работа с attachmentFrame
        
        let attachmentTop = postLabelFrame.size == CGSize.zero ? CartFeedConstants.postLabelInsets.top : moreTextButtonFrame.maxY + CartFeedConstants.postLabelInsets.bottom
        
        var attachmentFrame = CGRect(origin: CGPoint(x: 0, y: attachmentTop),
                                     size: CGSize.zero)
        
//        if let attachment = photoAttachment {
//            let photoHeight: Float = Float(attachment.height)
//            let photoWidth: Float = Float(attachment.width)
//            let ratio = CGFloat(photoHeight / photoWidth)
//
//            attachmentFrame.size = CGSize(width: cardViewWidth, height: cardViewWidth * ratio)
//        }
        
        if let attachment = photoAttachments.first {
            let photoHeight: Float = Float(attachment.height)
            let photoWidth: Float = Float(attachment.width)
            let ratio = CGFloat(photoHeight / photoWidth)
            if photoAttachments.count == 1 {
                attachmentFrame.size = CGSize(width: cardViewWidth, height: cardViewWidth * ratio)
            } else if photoAttachments.count > 1 {
                
                var photos = [CGSize]()
                for photo in photoAttachments {
                    let photoSize = CGSize(width: CGFloat(photo.width), height: CGFloat(photo.height))
                    photos.append(photoSize)
                }
                
                let rowHeight = CartRowLayout.rowHeightCounter(superviewWidth: cardViewWidth, photosArray: photos)
                attachmentFrame.size = CGSize(width: cardViewWidth, height: rowHeight!)
            }
        }
        
        // MARK: Работа с bottomViewFrame
        
        let bottomViewTop = max(postLabelFrame.maxY, attachmentFrame.maxY)
        
        let bottomViewFrame = CGRect(origin: CGPoint(x: 0, y: bottomViewTop),
                                     size: CGSize(width: cardViewWidth, height: CartFeedConstants.bottomViewHeight))
        
        // MARK: Работа с totalHeight
        
        let totalHeight = bottomViewFrame.maxY + CartFeedConstants.cardInsets.bottom
        
        return CartSizes(postLabelFrame: postLabelFrame,
                     moreTextButtonFrame: moreTextButtonFrame,
                     attachmentFrame: attachmentFrame,
                     bottomViewFrame: bottomViewFrame,
                     totalHeight: totalHeight)
    }
    

}
