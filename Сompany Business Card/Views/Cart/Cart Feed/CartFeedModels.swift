//
//  NewsfeedModels.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 15/03/2019.
//  Copyright (c) 2019 Алексей Пархоменко. All rights reserved.
//

import UIKit

enum CartFeed {
   
  enum Model {
    struct Request {
      enum RequestType {
        case getProducts
        case revealPostIds(postId: Int)
      }
    }
    struct Response {
      enum ResponseType {
        case presentProducts(feed: ProductsFeedResponse, revealdedPostIds: [Int])
        case presentFooterLoader
      }
    }
    struct ViewModel {
      enum ViewModelData {
        case displayNewsfeed(feedViewModel: CartFeedViewModel)
        case displayFooterLoader
      }
    }
  }
}

struct CartFeedViewModel {
    struct CartCell: CartFeedCellViewModel {
        var id: Int
        var name: String
        var text: String?
        var price: Double
        var photoAttachements: [CartFeedCellPhotoAttachementViewModel]
        var sizes: CartFeedCellSizes
    }
    
     struct CartFeedCellPhotoAttachment: CartFeedCellPhotoAttachementViewModel {
        var photoUrlString: String?
        var width: Int
        var height: Int
    }
    let cells: [CartCell]
    let footerTitle: String?
}
