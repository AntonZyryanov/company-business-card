//
//  NewsfeedInteractor.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 15/03/2019.
//  Copyright (c) 2019 Алексей Пархоменко. All rights reserved.
//

import UIKit

protocol CartFeedBusinessLogic {
  func makeRequest(request: CartFeed.Model.Request.RequestType)
}

class CartFeedInteractor: CartFeedBusinessLogic {

  var presenter: CartFeedPresentationLogic?
  var service: CartFeedService?
    
  func makeRequest(request: CartFeed.Model.Request.RequestType) {
    if service == nil {
      service = CartFeedService()
    }
    
    switch request {
    case .getProducts:
        service?.getFeed(completion: { [weak self] (revealedPostIds, feed) in
                print("R2D2 : \(feed.items.count)")
            DispatchQueue.main.async {
                self?.presenter?.presentData(response: CartFeed.Model.Response.ResponseType.presentProducts(feed: feed, revealdedPostIds: revealedPostIds)) 
            }
                 
        })
    case .revealPostIds(postId: let postId):
        print("Reveal post")
        service?.revealPostIds(forPostId: postId, completion: { revealedPostIds, feed in
            DispatchQueue.main.async {
                self.presenter?.presentData(response: CartFeed.Model.Response.ResponseType.presentProducts(feed: feed, revealdedPostIds: revealedPostIds))
            }
        })
    }
  }
}
