//
//  NewsfeedWorker.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 15/03/2019.
//  Copyright (c) 2019 Алексей Пархоменко. All rights reserved.
//

import UIKit
import SwiftUI

class CartFeedService {
    

    var networking: Networking
    var fetcher: DataFetcher
    
    private var revealedPostIds = [Int]()
    private var feedResponse: ProductsFeedResponse?
    
    init() {
        self.networking = NetworkService()
        self.fetcher = NetworkDataFetcher(networking: networking)
    }
    
    func getFeed(completion: @escaping ([Int], ProductsFeedResponse) -> Void) {
        var feedResponse = ProductsFeedResponse(items: OnlineShop.shared.chosenProducts)
        completion(self.revealedPostIds, feedResponse)
    }
    
    func revealPostIds(forPostId postId: Int, completion: @escaping ([Int], ProductsFeedResponse) -> Void) {
        revealedPostIds.append(postId)
        guard let feedResponse = self.feedResponse else { return }
        completion(revealedPostIds, feedResponse)
    }
    
}
