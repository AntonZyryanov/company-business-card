//
//  NewsfeedViewController.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 15/03/2019.
//  Copyright (c) 2019 Алексей Пархоменко. All rights reserved.
//

import UIKit
import SnapKit

protocol CartFeedDisplayLogic: class {
  func displayData(viewModel: CartFeed.Model.ViewModel.ViewModelData)
}

class CartFeedViewController: UIViewController, CartFeedDisplayLogic, CartFeedCodeCellDelegate, addToCartCellDelegate {
    
  static var shared = CartFeedViewController()
    
  var interactor: CartFeedBusinessLogic?
  var router: (NSObjectProtocol & CartfeedRoutingLogic)?
    
    private var feedViewModel = CartFeedViewModel.init(cells: [], footerTitle: nil)
  
  //  @IBOutlet weak var table: UITableView!
    
    var table = UITableView()
    
    private var titleView = CartTitleView()
    private lazy var footerView = CartFooterView()
    
    private var refreshControl: UIRefreshControl = {
       let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refreshControl
    }()
    
    // MARK: Setup
  
  private func setup() {
    let viewController        = self
    let interactor            = CartFeedInteractor()
    let presenter             = CartFeedPresenter()
    let router                = CartFeedRouter()
    viewController.interactor = interactor
    viewController.router     = router
    interactor.presenter      = presenter
    presenter.viewController  = viewController
    router.viewController     = viewController
  }
  
  // MARK: Routing
  

  
  // MARK: View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    setupBackground()
    setupTable()
   // setupTopBars()
    interactor?.makeRequest(request: CartFeed.Model.Request.RequestType.getProducts)
      navigationController?.navigationBar.barTintColor = UIColor(CartMediator.themeThirdColor)
      navigationController?.navigationBar.isHidden = true
      OnlineShop.shared.addToCartDelegate = self
      print("Puff Daddy")
  }
    
    
    private func setupTable() {
        self.view.addSubview(table)
        table.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
        
        let topInset: CGFloat = 8
        table.contentInset.top = topInset
        
      //  table.register(UINib(nibName: "NewsfeedCell", bundle: nil), forCellReuseIdentifier: NewsfeedCell.reuseId)
        table.register(CartFeedCodeCell.self, forCellReuseIdentifier: CartFeedCodeCell.reuseId)
        
        table.separatorStyle = .none
        table.backgroundColor = .orange
        
        table.addSubview(refreshControl)
        table.tableFooterView = footerView
        table.delegate = self
        table.dataSource = self
    }
    
    private func setupTopBars() {
        let topBar = UIView(frame: UIApplication.shared.statusBarFrame)
        topBar.backgroundColor = .white
        topBar.layer.shadowColor = UIColor.black.cgColor
        topBar.layer.shadowOpacity = 0.3
        topBar.layer.shadowOffset = CGSize.zero
        topBar.layer.shadowRadius = 8
        self.view.addSubview(topBar)
        
        self.navigationController?.hidesBarsOnSwipe = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.titleView = titleView
    }
    
    private func setupBackground() {
        self.view.backgroundColor  = UIColor(ProductsMediator.themeMainColor)
    }
    
    @objc private func refresh() {
        interactor?.makeRequest(request: CartFeed.Model.Request.RequestType.getProducts)
    }
  
    func addProductToCart(for cell: NewsfeedCodeCell) {
        print("Impala")
        refresh()
    }
    
  func displayData(viewModel: CartFeed.Model.ViewModel.ViewModelData) {

    switch viewModel {
    case .displayNewsfeed(let feedViewModel):
        self.feedViewModel = feedViewModel
        print("FEED RESPONSE: \(feedViewModel)")
        footerView.setTitle(feedViewModel.footerTitle)
      //  table.reloadData()
        table.reloadData()
        print("SKR \(table.cellForRow(at: IndexPath(row: 0, section: 0)))")
        print("SKR2 \(self.feedViewModel)")
        refreshControl.endRefreshing()
    case .displayFooterLoader:
        footerView.showLoader()
    }
  }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y > scrollView.contentSize.height / 1.1 {
          //  interactor?.makeRequest(request: ProductsFeed.Model.Request.RequestType.getNextBatch)
        }
    }
    
    // MARK: NewsfeedCodeCellDelegate
    
    func revealPost(for cell: CartFeedCodeCell) {
        guard let indexPath = table.indexPath(for: cell) else { return }
        let cellViewModel = feedViewModel.cells[indexPath.row]
        interactor?.makeRequest(request: CartFeed.Model.Request.RequestType.revealPostIds(postId: cellViewModel.id))
    }
}

extension CartFeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Tupac")
        return self.feedViewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: NewsfeedCell.reuseId, for: indexPath) as! NewsfeedCell
        let cell = tableView.dequeueReusableCell(withIdentifier: CartFeedCodeCell.reuseId, for: indexPath) as! CartFeedCodeCell
        let cellViewModel = feedViewModel.cells[indexPath.row]
        cell.set(viewModel: cellViewModel)
        cell.delegate = self
        print("DELEGATE \(cell.delegate)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellViewModel = feedViewModel.cells[indexPath.row]
        print("SHEESH \(cellViewModel.sizes.totalHeight)")
        return cellViewModel.sizes.totalHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellViewModel = feedViewModel.cells[indexPath.row]
        return cellViewModel.sizes.totalHeight
    }
}
