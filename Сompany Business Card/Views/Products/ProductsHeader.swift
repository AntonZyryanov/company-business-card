//
//  ProductsHeader.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 24.01.2022.
//

import SwiftUI

struct ProductsHeader: View {
    init (color: Color) {
        self.textColor = color
    }
    
    var textColor: Color = .white
    
    var body: some View {
        Text("Products").foregroundColor(textColor).font(.custom("Avenir-Book", size: 40))
    }
}

struct ProductsHeader_Previews: PreviewProvider {
    static var previews: some View {
       // ProductsHeader()
       Text("")
    }
}
