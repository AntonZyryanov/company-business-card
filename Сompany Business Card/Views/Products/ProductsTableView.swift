//
//  ProductsTableView.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 22.02.2022.
//

import UIKit
import SwiftUI
import MessageKit

// 1.
struct ProductsTableView: UIViewControllerRepresentable {
    
    @EnvironmentObject var userSettings: OnlineShopUserSettings
    
    // 2.
    func makeUIViewController(context: Context) -> UINavigationController {
        let productsFeedViewController = ProductsFeedViewController()
        let navigationViewController = UINavigationController(rootViewController: productsFeedViewController)
        return navigationViewController
    }
    
    // 3.
    func updateUIViewController(_ uiViewController: UINavigationController, context: Context) {
        print("")
    }
}
