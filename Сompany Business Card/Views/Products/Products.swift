import SwiftUI
import FirebaseAuth

struct Products: View {
    
    
    
    var themeMainColor: Color = ChatMediator.themeMainColor
    var themeSecondaryColor: Color = ProductsMediator.themeSecondaryColor
    var productsBackgroundColor: Color = ProductsMediator.productsBackgroundColor
    
    @EnvironmentObject var userSettings: OnlineShopUserSettings
    
    var body: some View {
        ZStack{
            themeMainColor.ignoresSafeArea()
            VStack{
                HStack{
                    Spacer().frame(width: 20)
                    ProductsHeader(color: themeSecondaryColor)
                    Spacer()
                }
                ProductsTableView()
            }
        }
    }
}

struct Products_Previews: PreviewProvider {
    static var previews: some View {
        Products()
    }
}
