//
//  NewsfeedCodeCell.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 31/03/2019.
//  Copyright © 2019 Алексей Пархоменко. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI
import SnapKit

protocol NewsfeedCodeCellDelegate: class {
    func revealPost(for cell: NewsfeedCodeCell)
}

protocol addToCartCellDelegate: class {
    func addProductToCart(for cell: NewsfeedCodeCell)
}

final class NewsfeedCodeCell: UITableViewCell {
    
    @EnvironmentObject var userSettings: OnlineShopUserSettings
    
    static let reuseId = "NewsfeedCodeCell"
    
    weak var delegate: NewsfeedCodeCellDelegate?
    weak var addToCartDelegate: addToCartCellDelegate?
    
    var id: Int = 0
    var price: Double = 0
    var photos: [Photo] = []
    
    // Первый слой
    
    let cardView: UIView = {
       let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // Второй слой
    
    let topView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let postlabel: UITextView = {
       let textView = UITextView()
        textView.font = Constants.postLabelFont
        textView.isScrollEnabled = false
        textView.isSelectable = true
        textView.isUserInteractionEnabled = true
        textView.isEditable = false
        
        let padding = textView.textContainer.lineFragmentPadding
        textView.textContainerInset = UIEdgeInsets.init(top: 0, left: -padding, bottom: 0, right: -padding)
        
        textView.dataDetectorTypes = UIDataDetectorTypes.all
        return textView
    }()
    
    lazy var moreTextButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        button.setTitleColor(UIColor(ProductsMediator.themeMainColor), for: .normal)
        button.contentHorizontalAlignment = .left
        button.contentVerticalAlignment = .center
        button.setTitle("show in full...", for: .normal)
        
        return button
    }()
    
    let galleryCollectionView = GalleryCollectionView()
    
    let postImageView: WebImageView = {
        let imageView = WebImageView()
        imageView.backgroundColor = #colorLiteral(red: 0.8234507442, green: 0.3115251064, blue: 0.3296223879, alpha: 1)
        return imageView
    }()
    
    let bottomView: UIView = {
       let view = UIView()
        return view
    }()
    
    // Третий слой на topView
    
    
    let nameLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 0.227329582, green: 0.2323184013, blue: 0.2370472848, alpha: 1)
        return label
    }()
    
    // Третий слой на bottomView
    
    let priceLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.numberOfLines = 1
        label.backgroundColor = .orange
        label.textColor = UIColor(ProductsMediator.themeSecondaryColor)
        label.textAlignment = .center
        return label
    }()
    
    let addToCardButton: UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        button.titleLabel?.numberOfLines = 1
        button.backgroundColor = UIColor(ProductsMediator.themeMainColor)
        button.titleLabel?.textColor = UIColor(ProductsMediator.themeSecondaryColor)
        button.setTitle("Add to cart", for: .normal)
        button.addTarget(self, action: #selector(addToCard), for: .touchUpInside)
        return button
    }()
    
    let cartImageView: UIImageView = {
       let cartImageView = UIImageView()
        cartImageView.image = UIImage(systemName: "cart.fill")
        cartImageView.tintColor = UIColor(ProductsMediator.themeSecondaryColor)
        return cartImageView
    }()
    
    
    override func prepareForReuse() {
        postImageView.set(imageURL: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        backgroundColor = .clear
        selectionStyle = .none
        
        cardView.layer.cornerRadius = 10
        cardView.clipsToBounds = true
        overlayFirstLayer() // первый слой
        overlaySecondLayer() // второй слой
        overlayThirdLayerOnTopView() // третий слой на topView
        overlayThirdLayerOnBottomView() // третий слой на bottomView
        overlayFourthLayerOnBottomViewViews() // четвертый слой на bottomViewViews
        moreTextButton.addTarget(self, action: #selector(moreTextButtonTouch), for: .touchUpInside)
        addToCardButton.addTarget(self, action: #selector(addToCard), for: .touchUpInside)
        self.isUserInteractionEnabled = true
        self.cardView.isUserInteractionEnabled = true
        self.moreTextButton.isUserInteractionEnabled = true
        self.moreTextButton.isEnabled = true
        
        
        self.isUserInteractionEnabled = true
        self.cardView.isUserInteractionEnabled = true
    }
    
    @objc func moreTextButtonTouch() {
        print("Nemtsov")
        delegate?.revealPost(for: self)
    }
    
    @objc func addToCard() {
        print("KAZAKY \(self.photos.count)")
        let productItem = ProductsFeedItem(id: self.id, name: self.nameLabel.text!, text: "", price: self.price, photos: OnlineShop.shared.photoArchive[self.id])
        OnlineShop.shared.chosenProducts.append(productItem!)
        OnlineShop.shared.numberOfChosenProducts += 1
        OnlineShop.shared.totalPrice += self.price
        self.addToCartDelegate = CartFeedViewController.shared
        print("KRIMINAL RUSSIA \(self.addToCartDelegate)")
        self.addToCartDelegate?.addProductToCart(for: self)
        self.addToCardButton.backgroundColor = .systemGreen
        self.addToCardButton.setTitle("Added to cart", for: .normal)
        self.cartImageView.isHidden = true
        self.addToCardButton.titleLabel?.textAlignment = .center
    }
    
    func set(viewModel: FeedCellViewModel) {
        self.id = viewModel.id
        self.price = viewModel.price
        
        nameLabel.text = viewModel.name
        postlabel.text = viewModel.text
        postlabel.frame = viewModel.sizes.postLabelFrame
        
        bottomView.frame = viewModel.sizes.bottomViewFrame
        moreTextButton.frame = viewModel.sizes.moreTextButtonFrame
        
        priceLabel.text = String(viewModel.price) + " $"
        
        if let photoAttachment = viewModel.photoAttachements.first, viewModel.photoAttachements.count == 1 {
            postImageView.isHidden = false
            galleryCollectionView.isHidden = true
            postImageView.set(imageURL: photoAttachment.photoUrlString)
            postImageView.frame = viewModel.sizes.attachmentFrame
            
            let photoAttachement = viewModel.photoAttachements.first
            var photos: [Photo] = []
            let photo = Photo(url: photoAttachment.photoUrlString!, width: photoAttachment.width, height: photoAttachment.height)
           // OnlineShop.shared.photoArchive[self.id]?.append(photo!)
            photos.append(photo!)
            OnlineShop.shared.photoArchive.updateValue(photos, forKey: self.id)
            print("Snitchy")
        } else if viewModel.photoAttachements.count > 1 {
            postImageView.isHidden = true
            galleryCollectionView.isHidden = false
            galleryCollectionView.frame = viewModel.sizes.attachmentFrame
            galleryCollectionView.set(photos: viewModel.photoAttachements)
        }
        else {
            postImageView.isHidden = true
            galleryCollectionView.isHidden = true
        }
    }
    
    private func overlayFourthLayerOnBottomViewViews() {
        addToCardButton.addSubview(cartImageView)
        cartImageView.snp.makeConstraints { make in
            make.height.equalTo(18)
            make.width.equalTo(18)
            make.trailing.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
        }
        
        addToCardButton.titleLabel?.snp.makeConstraints({ make in
            make.leading.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
        })
    }
    

    
    private func overlayThirdLayerOnBottomView() {
        
    }
    
    private func overlayThirdLayerOnTopView() {
        topView.addSubview(nameLabel)
        
        // nameLabel constraints
        nameLabel.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 0).isActive = true
        nameLabel.topAnchor.constraint(equalTo: topView.topAnchor, constant: 12).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: Constants.topViewHeight / 2 - 2).isActive = true
        bottomView.addSubview(priceLabel)
        bottomView.addSubview(addToCardButton)
        
        priceLabel.layer.cornerRadius = 10
        priceLabel.clipsToBounds = true
        
        priceLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
            make.width.equalTo(120)
            make.height.equalTo(40)
        }
        
        addToCardButton.layer.cornerRadius = 10
        addToCardButton.clipsToBounds = true

        addToCardButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
            make.width.equalTo(150)
            make.height.equalTo(40)
        }
    }
    
    
    
    private func overlaySecondLayer() {
        cardView.addSubview(topView)
        cardView.addSubview(postlabel)
        cardView.addSubview(moreTextButton)
        cardView.addSubview(postImageView)
        cardView.addSubview(galleryCollectionView)
        cardView.addSubview(bottomView)
        
        
        // topView constraints
        topView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        topView.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        topView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 8).isActive = true
        topView.heightAnchor.constraint(equalToConstant: Constants.topViewHeight).isActive = true

        
        // postlabel constraints
        // не нужно, так как размеры задаются динамически
        
        // moreTextButton constraints
        // не нужно, так как размеры задаются динамически
        
        // postImageView constraints
        // не нужно, так как размеры задаются динамически
        
        // bottomView constraints
        // не нужно, так как размеры задаются динамически
    }
    
     private func overlayFirstLayer() {
       // addSubview(cardView)
         contentView.addSubview(cardView)
        
        // cardView constraints
        cardView.fillSuperview(padding: Constants.cardInsets)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
