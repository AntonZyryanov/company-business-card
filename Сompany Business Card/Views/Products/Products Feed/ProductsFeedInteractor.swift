//
//  NewsfeedInteractor.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 15/03/2019.
//  Copyright (c) 2019 Алексей Пархоменко. All rights reserved.
//

import UIKit

protocol NewsfeedBusinessLogic {
  func makeRequest(request: ProductsFeed.Model.Request.RequestType)
}

class ProductsFeedInteractor: NewsfeedBusinessLogic {

  var presenter: NewsfeedPresentationLogic?
  var service: ProductsFeedService?
    
  func makeRequest(request: ProductsFeed.Model.Request.RequestType) {
    if service == nil {
      service = ProductsFeedService()
    }
    
    switch request {
    case .getProducts:
        service?.getFeed(completion: { [weak self] (revealedPostIds, feed) in
                print("R2D2 : \(feed.items.count)")
            DispatchQueue.main.async {
                self?.presenter?.presentData(response: ProductsFeed.Model.Response.ResponseType.presentProducts(feed: feed, revealdedPostIds: revealedPostIds)) 
            }
                 
        })
    case .revealPostIds(postId: let postId):
        print("Reveal post")
        service?.revealPostIds(forPostId: postId, completion: { revealedPostIds, feed in
            DispatchQueue.main.async {
                self.presenter?.presentData(response: ProductsFeed.Model.Response.ResponseType.presentProducts(feed: feed, revealdedPostIds: revealedPostIds))
            }
        })
    }
  }
}
