//
//  NewsfeedViewController.swift
//  VKNewsFeed
//
//  Created by Алексей Пархоменко on 15/03/2019.
//  Copyright (c) 2019 Алексей Пархоменко. All rights reserved.
//

import UIKit
import SnapKit

protocol NewsfeedDisplayLogic: class {
  func displayData(viewModel: ProductsFeed.Model.ViewModel.ViewModelData)
}

class ProductsFeedViewController: UIViewController, NewsfeedDisplayLogic, NewsfeedCodeCellDelegate {
    
    
  static var shared = ProductsFeedViewController()
    
  var interactor: NewsfeedBusinessLogic?
  var router: (NSObjectProtocol & NewsfeedRoutingLogic)?
    
    private var feedViewModel = FeedViewModel.init(cells: [], footerTitle: nil)
  
  //  @IBOutlet weak var table: UITableView!
    
    var table = UITableView()
    
    private var titleView = TitleView()
    private lazy var footerView = FooterView()
    
    private var refreshControl: UIRefreshControl = {
       let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refreshControl
    }()
    
    // MARK: Setup
  
  private func setup() {
    let viewController        = self
    let interactor            = ProductsFeedInteractor()
    let presenter             = ProductsFeedPresenter()
    let router                = ProductsFeedRouter()
    viewController.interactor = interactor
    viewController.router     = router
    interactor.presenter      = presenter
    presenter.viewController  = viewController
    router.viewController     = viewController
  }
  
  // MARK: Routing
  

  
  // MARK: View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    setupBackground()
    setupTable()
    setupTopBars()
    
    interactor?.makeRequest(request: ProductsFeed.Model.Request.RequestType.getProducts)
    
  }
    
    private func setupTable() {
        self.view.addSubview(table)
        table.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
        
        let topInset: CGFloat = 8
        table.contentInset.top = topInset
        
      //  table.register(UINib(nibName: "NewsfeedCell", bundle: nil), forCellReuseIdentifier: NewsfeedCell.reuseId)
        table.register(NewsfeedCodeCell.self, forCellReuseIdentifier: NewsfeedCodeCell.reuseId)
        
        table.separatorStyle = .none
        table.backgroundColor = .orange
        
        table.addSubview(refreshControl)
        table.tableFooterView = footerView
        table.delegate = self
        table.dataSource = self
    }
    
    private func setupTopBars() {
        let topBar = UIView(frame: UIApplication.shared.statusBarFrame)
        topBar.backgroundColor = .white
        topBar.layer.shadowColor = UIColor.black.cgColor
        topBar.layer.shadowOpacity = 0.3
        topBar.layer.shadowOffset = CGSize.zero
        topBar.layer.shadowRadius = 8
        self.view.addSubview(topBar)
        
        self.navigationController?.hidesBarsOnSwipe = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.titleView = titleView
    }
    
    private func setupBackground() {
        self.view.backgroundColor  = UIColor(ProductsMediator.themeMainColor)
    }
    
    @objc private func refresh() {
        interactor?.makeRequest(request: ProductsFeed.Model.Request.RequestType.getProducts)
    }
    
    
  
  func displayData(viewModel: ProductsFeed.Model.ViewModel.ViewModelData) {

    switch viewModel {
    case .displayNewsfeed(let feedViewModel):
        self.feedViewModel = feedViewModel
        print("FEED RESPONSE: \(feedViewModel)")
        footerView.setTitle(feedViewModel.footerTitle)
      //  table.reloadData()
        table.reloadData()
        print("SKR \(table.cellForRow(at: IndexPath(row: 0, section: 0)))")
        print("SKR2 \(self.feedViewModel)")
        refreshControl.endRefreshing()
    case .displayFooterLoader:
        footerView.showLoader()
    }
  }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y > scrollView.contentSize.height / 1.1 {
          //  interactor?.makeRequest(request: ProductsFeed.Model.Request.RequestType.getNextBatch)
        }
    }
    
    // MARK: NewsfeedCodeCellDelegate
    
    func revealPost(for cell: NewsfeedCodeCell) {
        guard let indexPath = table.indexPath(for: cell) else { return }
        let cellViewModel = feedViewModel.cells[indexPath.row]
        interactor?.makeRequest(request: ProductsFeed.Model.Request.RequestType.revealPostIds(postId: cellViewModel.id))
    }
}

extension ProductsFeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Tupac")
        return self.feedViewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: NewsfeedCell.reuseId, for: indexPath) as! NewsfeedCell
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsfeedCodeCell.reuseId, for: indexPath) as! NewsfeedCodeCell
        let cellViewModel = feedViewModel.cells[indexPath.row]
        cell.set(viewModel: cellViewModel)
        cell.delegate = self
        print("DELEGATE \(cell.delegate)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellViewModel = feedViewModel.cells[indexPath.row]
        print("SHEESH \(cellViewModel.sizes.totalHeight)")
        return cellViewModel.sizes.totalHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellViewModel = feedViewModel.cells[indexPath.row]
        return cellViewModel.sizes.totalHeight
    }
}
