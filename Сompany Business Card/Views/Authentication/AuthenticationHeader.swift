//
//  AuthenticationHeader.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 30.01.2022.
//

import SwiftUI

struct AuthenticationHeader: View {
    
    var themeSecondaryColor = AuthenticationMediator.themeSecondaryColor
    
    var headersFont : Font = .largeTitle
    
    init(themeSecondaryColor: Color,headersFont: Font) {
        self.themeSecondaryColor = themeSecondaryColor
        self.headersFont = headersFont
    }
    
    var body: some View {
        Text("Welcome!").foregroundColor(themeSecondaryColor).font(.custom("Avenir-Book", size: 40))
    }
}

struct AuthenticationHeader_Previews: PreviewProvider {
    static var previews: some View {
        //AuthenticationHeader()
        Text("")
    }
}
