//
//  Authentication.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 30.01.2022.
//

import SwiftUI

struct Authentication: View {
    
    
    init() {
        
    }
    
    @Environment(\.presentationMode) var presentationMode
    
    
    
    @State var themeMainColor = AuthenticationMediator.themeMainColor
    @State var themeSecondaryColor = AuthenticationMediator.themeSecondaryColor
    
    
    @State var headersFont = AuthenticationMediator.headersFont
    @State var miniHeadersFont = AuthenticationMediator.miniHeadersFont
    @State var textFont = AuthenticationMediator.textFont
    
    @State var userHasAnAccount = true
    
    @State var userLogedIn : Bool = false

    @EnvironmentObject var onlineShopUserSettings: OnlineShopUserSettings
    
    var body: some View {
        ZStack{
            themeMainColor.ignoresSafeArea()
            if userHasAnAccount {
                LoginView(themeMainColor: $themeMainColor, themeSecondaryColor: $themeSecondaryColor, headersFont: $headersFont, miniHeadersFont: $miniHeadersFont, textFont: $textFont, userHasAnAccount: $userHasAnAccount, userLogedIn: $userLogedIn)
            }else{
                SignUpView(themeMainColor: $themeMainColor, themeSecondaryColor: $themeSecondaryColor, headersFont: $headersFont, miniHeadersFont: $miniHeadersFont, textFont: $textFont, userHasAnAccount: $userHasAnAccount, userLogedIn: $userLogedIn)
            }
            
        }.onChange(of: userLogedIn) { newValue in
            if newValue == true{
                presentationMode.wrappedValue.dismiss()
            }
            
        }
        
    }
}

struct Authentication_Previews: PreviewProvider {
    static var previews: some View {
        Authentication()
    }
}
