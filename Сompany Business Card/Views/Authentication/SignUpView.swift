//
//  SignUpView.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 31.01.2022.
//

import SwiftUI

struct SignUpView: View {
    
    @State private var email: String = ""
    
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    
    @State var loginError = false
    
    @Binding var themeMainColor : Color
    @Binding var themeSecondaryColor : Color
    
    
    @Binding var headersFont : Font
    @Binding var miniHeadersFont : Font
    @Binding var textFont : Font
    
    var alertTitle: String = ""
    @State var alertText: String = ""
    
    @Binding var userHasAnAccount: Bool
    
    @Binding var userLogedIn : Bool
    
    @EnvironmentObject var onlineShopUserSettings: OnlineShopUserSettings
    
    var body: some View {
        VStack{
            Spacer().frame(height: 150)
            AuthenticationHeader(themeSecondaryColor: themeSecondaryColor, headersFont: headersFont)
            Spacer()
            SignUpStackView(email: $email, password: $password, loginError: $loginError, themeMainColor: $themeMainColor, themeSecondaryColor: $themeSecondaryColor, headersFont: $headersFont, miniHeadersFont: $miniHeadersFont, textFont: $textFont, alertText: $alertText, userHasAnAccount: $userHasAnAccount, userLogedIn: $userLogedIn, confirmPassword: $confirmPassword)
            .alert(isPresented: $loginError) { () -> Alert in
                Alert(title: Text(alertTitle), message: Text(alertText), dismissButton: .default(Text("Ok")) )
                    }
            Spacer().frame(height: 80)
            Text("Do you have an account?")
                .foregroundColor(themeSecondaryColor)
                .font(.custom("Avenir-Book", size: 18))
            Button(action: {
                userHasAnAccount = true
            }) {
                Text("Login")
                    .foregroundColor(themeSecondaryColor)
                    .font(.custom("Avenir-Book", size: 18))
            }
            Spacer().frame(height: 40)
        }
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
