//
//  SignUpStackView.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 31.01.2022.
//

import SwiftUI

struct SignUpStackView: View {
    
    @Binding var email: String
    
    @Binding var password: String
    
    @Binding var loginError: Bool
    
    @Binding var themeMainColor : Color
    @Binding var themeSecondaryColor : Color
    
    
    @Binding var headersFont : Font
    @Binding var miniHeadersFont : Font
    @Binding var textFont : Font
    
    var alertTitle: String = ""
    @Binding var alertText: String
    
    @Binding var userHasAnAccount: Bool
    
    @Binding var userLogedIn : Bool
    
    @Binding var confirmPassword : String
    
    @EnvironmentObject var onlineShopUserSettings: OnlineShopUserSettings
    
    var body: some View {
        VStack{
            TextField("e-mail", text: $email)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .border(themeSecondaryColor, width: 2)
                .padding(.horizontal, 40)
            TextField("password", text: $password)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .border(themeSecondaryColor, width: 2)
                .padding(.horizontal, 40)
            TextField("Confirm password", text: $confirmPassword)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .border(themeSecondaryColor, width: 2)
                .padding(.horizontal, 40)
            Button(action: {
                AuthService.shared.register(email: email, password: password, confirmPassword: confirmPassword) { result in
                    switch result{
                        
                    case .success(_):
                        print("Prodigy")
                        FirestoreService.shared.saveProfileWith(email: email) { result in
                            switch result {
                            case .success(let user):
                                print("Havoc")
                                FirestoreService.shared.setCurrentUser(currentUser: user)
                                let chat = MChat(friendUsername: OnlineShop.shared.consultant.username, friendAvatarStringURL: OnlineShop.shared.consultant.avatarStringURL, friendId: OnlineShop.shared.consultant.id, lastMessageContent: "")
                                onlineShopUserSettings.currentUser = user
                                print("Dre")
                                FirestoreService.shared.createActiveChat(chat: chat, messages: []) { result in
                                    onlineShopUserSettings.isAuthenticationNeeded = false
                                    onlineShopUserSettings.loggedIn = true
                                    userLogedIn = true
                                    switch result {
                                    case .success(_):
                                        print("success")
                                        OnlineShop.shared.chatWithConsultant = chat
                                    case .failure(let error):
                                        print("2pac")
                                        self.alertText = error.localizedDescription
                                        loginError = true
                                    }
                                }
                                onlineShopUserSettings.isAuthenticationNeeded = false
                                onlineShopUserSettings.loggedIn = true  //WARNING: THIS NEEDS TO BE IN CLOSURE CREATEACTIVECHAT
                                userLogedIn = true
                                print("Eazy")
                            case .failure(let error):
                                print("Ren")
                                self.alertText = error.localizedDescription
                                loginError = true
                            }
                        }
                        
                    case .failure(let error):
                        print("Failure")
                        self.alertText = error.localizedDescription
                        loginError = true
                    }
                }
                
                //presentationMode.wrappedValue.dismiss()
            }) {
                Text("Sign up")
                    .foregroundColor(themeSecondaryColor)
                    .font(.custom("Avenir-Book", size: 18))
            }
        }
    }
}

struct SignUpStackView_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
