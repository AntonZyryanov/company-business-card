//
//  LoginButton.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 30.01.2022.
//

import SwiftUI

struct LoginStackView: View {
    
    @Binding var email: String
    
    @Binding var password: String
    
    @Binding var loginError: Bool
    
    @Binding var themeMainColor : Color
    @Binding var themeSecondaryColor : Color
    
    
    @Binding var headersFont : Font
    @Binding var miniHeadersFont : Font
    @Binding var textFont : Font
    
    var alertTitle: String = ""
    @Binding var alertText: String
    
    @Binding var userHasAnAccount: Bool
    
    @Binding var userLogedIn : Bool
    
    @EnvironmentObject var onlineShopUserSettings: OnlineShopUserSettings
    
    
    var body: some View {
        VStack{
            TextField("e-mail", text: $email)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .border(themeSecondaryColor, width: 2)
                .padding(.horizontal, 40)
            TextField("password", text: $password)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .border(themeSecondaryColor, width: 2)
                .padding(.horizontal, 40)
            Button(action: {
                AuthService.shared.login(email: email, password: password) { result in
                    switch result{
                        
                    case .success(let user):
                        FirestoreService.shared.getUserData(user: user) { result in
                            switch result {
                            case .success(let muser):
                                FirestoreService.shared.setCurrentUser(currentUser: muser)
                                print("succes")
                                onlineShopUserSettings.isAuthenticationNeeded = false
                                onlineShopUserSettings.loggedIn = true
                                onlineShopUserSettings.currentUser = muser
                                userLogedIn = true
                            case .failure(let error):
                                self.alertText = error.localizedDescription
                                loginError = true
                            }
                        }
                        
                    case .failure(let error):
                        print("Failure")
                        self.alertText = error.localizedDescription
                        loginError = true
                    }
                }
            }) {
                Text("Login")
                    .foregroundColor(themeSecondaryColor)
                    .font(.custom("Avenir-Book", size: 18))
            }
        }
    }
}

struct LoginButton_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
