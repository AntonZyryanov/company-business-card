//
//  LoginView.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 31.01.2022.
//

import SwiftUI

struct LoginView: View {
    
    
    @State private var email: String = ""
    
    @State private var password: String = ""
    
    @State var loginError = false
    
    @Binding var themeMainColor : Color
    @Binding var themeSecondaryColor : Color
    
    
    @Binding var headersFont : Font
    @Binding var miniHeadersFont : Font
    @Binding var textFont : Font
    
    var alertTitle: String = ""
    @State var alertText: String = ""
    
    @Binding var userHasAnAccount: Bool
    
    @Binding var userLogedIn : Bool
    
    @EnvironmentObject var onlineShopUserSettings: OnlineShopUserSettings
    
    var body: some View {
        VStack{
            Spacer().frame(height: 150)
            AuthenticationHeader(themeSecondaryColor: themeSecondaryColor, headersFont: headersFont)
            Spacer()
            LoginStackView(email: $email, password: $password, loginError: $loginError, themeMainColor: $themeMainColor, themeSecondaryColor: $themeSecondaryColor, headersFont: $headersFont, miniHeadersFont: $miniHeadersFont, textFont: $textFont, alertText: $alertText, userHasAnAccount: $userHasAnAccount, userLogedIn: $userLogedIn)
            .alert(isPresented: $loginError) { () -> Alert in
                Alert(title: Text(alertTitle), message: Text(alertText), dismissButton: .default(Text("Ok")) )
                    }
            Spacer().frame(height: 80)
            GoogleView(themeSecondaryColor: themeSecondaryColor)
            Text("You don't have an account?")
                .foregroundColor(themeSecondaryColor)
                .font(.custom("Avenir-Book", size: 18))
            Button(action: {
                userHasAnAccount = false
            }) {
                Text("Sign up")
                    .foregroundColor(themeSecondaryColor)
                    .font(.custom("Avenir-Book", size: 18))
            }
            Spacer().frame(height: 40)
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
