//
//  googleView.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 31.01.2022.
//

import SwiftUI

struct GoogleView: View {
    
    init(themeSecondaryColor: Color){
        self.themeSecondaryColor = themeSecondaryColor
    }
    
    var themeSecondaryColor = Color.white
    
    var body: some View {
        HStack{
            Text("Continue with")
                .foregroundColor(themeSecondaryColor)
                .font(.custom("Avenir-Book", size: 18))
            Image("googleLogo")
        }
    }
}

struct googleView_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
