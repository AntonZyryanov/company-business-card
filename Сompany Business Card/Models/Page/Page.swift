//
//  Page.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 17.01.2022.
//

import Foundation


class Page {
    var header: Header?
    var body: Body?
}
