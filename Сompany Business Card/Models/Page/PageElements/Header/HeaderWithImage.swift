import Foundation
import UIKit


protocol HeaderWithImage: Header {
    var headerImage: UIImage { get set }
}
