//
//  Header.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 17.01.2022.
//

import Foundation
import UIKit


protocol Header {
    var headerText: UITextView { get set }
}
