//
//  CustomHeader.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 17.01.2022.
//

import Foundation
import UIKit

protocol CustomHeader: Header {
    var customView : UIView { get set }
}
