//
//  UserSettings.swift
//  Сompany Business Card
//
//  Created by TonyMontana on 31.01.2022.
//

import Foundation
import SwiftUI

class OnlineShopUserSettings: ObservableObject {
    
    @Published var loggedIn = false
    
    @Published var currentUser : MUser = MUser(username: "_", email: "_", avatarStringURL: "_", description: "_", sex: "_", id: "_")
    
    @Published var chatWithConsultant : MChat = MChat(friendUsername: "_", friendAvatarStringURL: "_", friendId: "Consultant1", lastMessageContent: "")
    
    
    @Published  var isAuthenticationNeeded = false
    
    @Published var currentChat : MChat = MChat(friendUsername: "_", friendAvatarStringURL: "_", friendId: "Consultant1", lastMessageContent: "")
    
    @Published var addToCartDelegate: addToCartCellDelegate?
    
    @Published var chosenProducts: [ProductsFeedItem] = []
    
    @Published var numberOfChosenProducts: Int = 0
}
